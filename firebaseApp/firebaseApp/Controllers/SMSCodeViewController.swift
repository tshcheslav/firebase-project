//
//  SMSCodeViewController.swift
//  firebaseApp
//
//  Created by 1 on 30.11.2021.
//

import UIKit

class SMSCodeViewController: UIViewController, UITextFieldDelegate {

    private let codefield: UITextField = {
       let field = UITextField()
        field.backgroundColor = .secondarySystemBackground
        field.placeholder = "Ener Code"
        field.returnKeyType = .continue
        field.textAlignment = .center
        return field
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        view.addSubview(codefield)
        codefield.frame = CGRect(x: 0, y: 0, width: 220, height: 50)
        codefield.center = view.center
        codefield.delegate = self
    }


    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if let text = textField.text , !text.isEmpty {
            let code = text
            AuthManager.shared.verifyCode(smsCode: code) { [weak self] success in
                guard success else {return}
                DispatchQueue.main.async {
                    let vc = AccountViewController()
                    vc.modalPresentationStyle = .fullScreen
                    self?.present(vc, animated: true)
                }
            }
        }
        return true
    }
}
